package com.bw.gamllapi.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Setter;

@Builder
@Data
@AllArgsConstructor
public class KeyWord {
    private String name;
    private Integer value;

}
