package com.bw.gamllapi.service.impl;

import com.bw.gamllapi.mapper.OrderMapper;
import com.bw.gamllapi.pojo.A;
import com.bw.gamllapi.pojo.KeyWord;
import com.bw.gamllapi.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service
public class OrderServiceImpl  implements IOrderService {
    @Autowired
    private OrderMapper orderMapper;

    @Override
    public String getGmv(String date) {
        // 可以做逻辑判断
        if(orderMapper.getGmv(date)== null){
            return "0";
        }else{
            return orderMapper.getGmv(date);
        }
    }

    @Override
    public List<KeyWord> getKeyWord(String date) {
        return orderMapper.getKeyWord(date);
    }

    @Override
    public List<A> getA(String date, String vc, String ch, String ar) {
        return orderMapper.getA(date,vc,ch,ar);
    }
}
