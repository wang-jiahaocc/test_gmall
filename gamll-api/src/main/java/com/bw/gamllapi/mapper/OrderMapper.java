package com.bw.gamllapi.mapper;

import com.bw.gamllapi.pojo.A;
import com.bw.gamllapi.pojo.KeyWord;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface OrderMapper {
    @Select("select sum(original_amount) from dws_trade_sku_order_window where cur_date=#{date}")
    String getGmv(String date);



    @Select("select word as name ,count as value from word where  cur_date=#{date}")
    List<KeyWord> getKeyWord(String date);

    @Select("select vc,ch,ar, sum(uv_ct) uv_ct,sum(sv_ct) sv_ct,sum(pv_ct) pv_ct,sum(dur_sum) dur_sum from dws_traffic_vc_ch_ar_is_new_page_view_window  where cur_date=#{date} and vc=#{vc} and ch=#{ch}  and  ar=#{ar} group by vc,ch,ar")
    List<A> getA(@Param("date") String date, @Param("vc") String vc, @Param("ch") String ch, @Param("ar") String ar);

}
