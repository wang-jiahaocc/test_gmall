package com.bw.gmall.realtime.dwd.db.split.app;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

public class Test2 {
    public static void main(String[] args) {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        StreamTableEnvironment tableEnv  = StreamTableEnvironment.create(env);

        tableEnv.executeSql("CREATE TABLE test3 (\n" +
                "  `i` STRING,\n" +
                "  `s` BIGINT\n" +
                ") WITH (\n" +
                "  'connector' = 'kafka',\n" +
                "  'topic' = 'my_test_top_2',\n" +
                "  'properties.group.id' = 'testGroup1',\n" +
                "  'scan.startup.mode' = 'latest-offset',\n" +
                "  'properties.bootstrap.servers' = 'hadoop102:9092',\n" +
                "  'format' = 'json'\n" +
                ")");
        tableEnv.sqlQuery("select i,sum(s) from test3 group by i").execute().print();
        Table table = tableEnv.sqlQuery("select i,sum(s) from test3 group by i");
//        Table table = tableEnv.sqlQuery("select i,s from test3 ");
//        tableEnv.sqlQuery("select i,s from test3").execute().print();

        tableEnv.executeSql("CREATE TABLE test (\n" +
                "  i STRING,\n" +
                "  v BIGINT,\n" +
                "  PRIMARY KEY (i) NOT ENFORCED\n" +
                ") WITH (\n" +
                "   'connector' = 'jdbc',\n" +
                "   'url' = 'jdbc:mysql://hadoop102:3306/Test?useSSL=false',\n" +
                "   'table-name' = 'test',\n" +
                "   'driver' = 'com.mysql.cj.jdbc.Driver',\n" +
                "   'username' = 'root',\n" +
                "   'password' = '123456'\n" +
                ")");
        table.insertInto("test").execute();
    }
}
