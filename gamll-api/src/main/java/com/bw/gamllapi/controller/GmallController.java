package com.bw.gamllapi.controller;


import com.bw.gamllapi.pojo.A;
import com.bw.gamllapi.pojo.KeyWord;
import com.bw.gamllapi.service.IOrderService;
import com.bw.gamllapi.util.DateFormatUtil;
import com.bw.gamllapi.util.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

@RequestMapping("/gmall/api")
@RestController
public class GmallController {
    @Autowired
    private IOrderService orderService;

    @RequestMapping("/get_gmv")
    public String getGmv(@RequestParam(value = "date",defaultValue = "0") String date){
        if ("0".equals(date)){
            date = DateFormatUtil.getNowDate();
        }
        System.out.println("date = " + date);
        /**
         * {
         *     "status":0  0代表成功 -1代表失败
         *     "msg":""
         *     "data":{} []
         * }
         */
        String gmv = orderService.getGmv(date);
        return "{\n" +
                "  \"status\": 0,\n" +
                "  \"msg\": \"\",\n" +
                "  \"data\": "+gmv+"\n" +
                "}";
    }


    @RequestMapping("/get_keyword")
    public R<List<KeyWord>> getKeyWord(@RequestParam(value = "date",defaultValue = "0") String date){
        if ("0".equals(date)){
            date = DateFormatUtil.getNowDate();
        }
        List<KeyWord> keyWords = orderService.getKeyWord(date);
        return R.success(keyWords);
    }

    @RequestMapping("/get_a")
    public String getA(@RequestParam(value = "date",defaultValue = "0") String date,
                           @RequestParam(value = "vc",defaultValue = "0") String vc,
                           @RequestParam(value = "ch",defaultValue = "0") String ch,
                           @RequestParam(value = "ar",defaultValue = "0") String ar){
        if ("0".equals(date)){
            date = DateFormatUtil.getNowDate();
        }
        List<A> keyWords = orderService.getA(date,vc,ch,ar);
        return "{\n" +
                "\t\"status\": 0,\n" +
                "\t\"msg\": \"\",\n" +
                "\t\"data\": {\n" +
                "\t\t\"categories\": [\""+date+"\"],\n" +
                "\t\t\"series\": [{\n" +
                "\t\t\t\"name\": \"独立访客数\",\n" +
                "\t\t\t\"data\": ["+keyWords.get(0).getUv_ct()+"]\n" +
                "\t\t}, {\n" +
                "\t\t\t\"name\": \"会话数\",\n" +
                "\t\t\t\"data\": ["+keyWords.get(0).getSv_ct()+"]\n" +
                "\t\t}, {\n" +
                "\t\t\t\"name\": \"页面浏览数\",\n" +
                "\t\t\t\"data\": ["+keyWords.get(0).getPv_ct()+"]\n" +
                "\t\t}, {\n" +
                "\t\t\t\"name\": \"累计访问时长\",\n" +
                "\t\t\t\"data\": ["+keyWords.get(0).getDur_sum()+"]\n" +
                "\t\t}]\n" +
                "\t}\n" +
                "}";



    }

}
