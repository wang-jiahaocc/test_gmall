package com.bw.gmall.realtime.common.function;

import com.alibaba.fastjson.JSONObject;

public interface IDimAsync<T> {

   String getDimId(T t);
   void setDim(T t, JSONObject dimInfo);
}
