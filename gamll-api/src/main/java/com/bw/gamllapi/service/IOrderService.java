package com.bw.gamllapi.service;

import com.bw.gamllapi.pojo.A;
import com.bw.gamllapi.pojo.KeyWord;

import java.util.List;

public interface IOrderService {
    String getGmv(String date);
    List<KeyWord> getKeyWord(String date);
    List<A> getA(String date, String vc, String ch, String ar);
}
