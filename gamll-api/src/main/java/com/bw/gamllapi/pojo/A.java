package com.bw.gamllapi.pojo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
@AllArgsConstructor
public class A {
    private String vc;
    private String ch;
    private String ar;
    private Integer uv_ct;
    private Integer sv_ct;
    private Integer pv_ct;
    private Integer dur_sum;
}
