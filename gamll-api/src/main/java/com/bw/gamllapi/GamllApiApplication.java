package com.bw.gamllapi;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@MapperScan("com.bw.gamllapi.mapper")
@SpringBootApplication
public class GamllApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(GamllApiApplication.class, args);
    }

}
