package com.bw.gmall.realtime.dwd.db.split.app;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.Table;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

public class Test1 {
    public static void main(String[] args) {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);

        StreamTableEnvironment tableEnv  = StreamTableEnvironment.create(env);

        tableEnv.executeSql("CREATE TABLE test2 (\n" +
                "  `i` STRING,\n" +
                "  `s` BIGINT,\n" +
                "  PRIMARY KEY (i) NOT ENFORCED \n" +
                ") WITH (\n" +
                "  'connector' = 'upsert-kafka',\n" +
                "  'topic' = 'my_test_top_2',\n" +
                "  'properties.group.id' = 'testGroup',\n" +
                "  'properties.bootstrap.servers' = 'hadoop102:9092',\n" +
                "  'key.format' = 'json',\n" +
                "  'value.format' = 'json'\n" +
                ")");
//        tableEnv.sqlQuery("select i,s from test2 ").execute().print();
        tableEnv.sqlQuery("select i,sum(s) from test2 group by i").execute().print();
        Table table = tableEnv.sqlQuery("select i,sum(s) from test2 group by i");
//        Table table = tableEnv.sqlQuery("select i,s from test2 ");


        tableEnv.executeSql("CREATE TABLE test (\n" +
                "  i STRING,\n" +
                "  v BIGINT,\n" +
                "  PRIMARY KEY (i) NOT ENFORCED\n" +
                ") WITH (\n" +
                "   'connector' = 'jdbc',\n" +
                "   'url' = 'jdbc:mysql://hadoop102:3306/Test?useSSL=false',\n" +
                "   'table-name' = 'test',\n" +
                "   'driver' = 'com.mysql.cj.jdbc.Driver',\n" +
                "   'username' = 'root',\n" +
                "   'password' = '123456'\n" +
                ")");
        table.insertInto("test").execute();

//        tableEnv.executeSql("CREATE TABLE test (\n" +
//                "  `id` STRING,\n" +
//                "  `ts` BIGINT,\n" +
//                "  `vc` BIGINT\n" +
//                ") WITH (\n" +
//                "  'connector' = 'kafka',\n" +
//                "  'topic' = 'my_test_top_1',\n" +
//                "  'properties.bootstrap.servers' = 'hadoop102:9092',\n" +
//                "  'properties.group.id' = 'testGroup',\n" +
//                "  'scan.startup.mode' = 'latest-offset',\n" +
//                "  'format' = 'json'\n" +
//                ")");

//        Table table = tableEnv.sqlQuery("select id,sum(vc) from test group by id");




//        table.insertInto("test1").execute();
    }
}
